<?php
include_once 'dbconfig.php';

class DB{
    private static $instance;
    private $pdo;

    private function __construct(){
        try{
            $this->pdo=new PDO('mysql:host='.DBHOST.';dbname='.DBNAME,DBUSER,DBPASS);
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_WARNING);
        }catch (PDOException $ex){
            die($ex->getMessage());
        }
    }

    public static function getInstance(){
        if(!isset(self::$instance)){
            self::$instance=new DB();
        }
        return self::$instance;
    }

    /**
     * @return PDO
     */
    public function getPdo()
    {
        return $this->pdo;
    }
}