<!DOCTYPE html>
<html>
	<head>
		<title>Online Ticket</title>
		<link rel="stylesheet" type="text/css" href="css/onlineticket.css">
	</head>
	<body>
		<header>
			<div class = "container">
				<div id = "branding">
					<h1><span class ="highlight">Online</span>Ticket</h1>
				</div>	
				<nav>
					<ul>
						<li class = "current"><a href = "#">Home</a></li>
						<li><a href = "about.php">About</a></li>
						<li><a href = "contact.php">Contact</a></li>

					</ul>

				</nav>
			</div>	

		</header>

		<section id = "showcase">
				<div class="wrap">
						<div id="arrow-left" class="arrow"></div>
						<div id="slider">
						  <div class="slide slide1">
							<div class="slide-content">
							  <span>Image One</span>
							</div>
						  </div>
						  <div class="slide slide2">
							<div class="slide-content">
							  <span>Image Two</span>
							</div>
						  </div>
						  <div class="slide slide3">
							<div class="slide-content">
							  <span>Image Three</span>
							</div>
						  </div>
						</div>
						<div id="arrow-right" class="arrow"></div>
					  </div>

		</section>
		<script src="./js/slider.js"></script>

		<section id = "newslatter" >
			<div class = "container">
				<h1></h1>
				<form >
					<input type="text" placeholder ="Kerko...">
					<button type ="submit" class = "button_1">Submit</button>

				</form>

			</div>	
		</section>

		<section id = "boxes">
			<div class = "container">
				<div id="boxes">
					<div class ="box">
						<a href = "football.php"><img src="./img/football.png"></a>
						<h3>Football Tickets</h3>
						
					</div>
					<div class ="box">
						<a href = "concert.php"><img src="./img/concert.png"></a>
						<h3>Concerts Tickets</h3>
					</div>
				</div>	
			</div> 
		</section>

		<footer>
			<p>Online Ticket, Copyright &copy; 2019</p>

		</footer>


	<scrip src="dom.js"></scrip>
	</body>

</html>