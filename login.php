<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://bootswatch.com/4/lumen/bootstrap.min.css">
    <style>
        .forma{
            margin-top:5vh;
        }
    </style>
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-sm-4"></div>
            <div class="col-sm-2"><h1 style="margin-top: 10vh">Login</h1></div>
        </div>
        <div class="row">
            <div class="col-sm-4"></div>
            <div class="col-sm-3">
                <form class="forma" action="login.php" method="post">
                    <label for="">Username</label>
                    <input required type="text" name="username" class="form-control">
                    <br>
                    <label for="">Password</label>
                    <input required type="password" name="password"  class="form-control">
                    <br>
                    <input type="submit" class="btn btn-primary">
                </form>
            </div>
        </div>
    </div>
</body>
<?php
include_once 'autoinclude.php';


if(isset($_POST['username'])&&isset($_POST['password'])){
    if(!empty($_POST['username'])&&!empty($_POST['password'])){
        $username=$_POST['username'];
        $password=$_POST['password'];
        $db=DB::getInstance();
        $sql="SELECT id,username,password FROM users WHERE username=? and password=?";
        $statement=$db->getPdo()->prepare($sql);
        $statement->bindValue(1,$username);
        $statement->bindValue(2,$password);
        if($statement->execute()){
            if($statement->rowCount()==1){
                header("Location:onlineticket.php");
            }else{
                echo "<br>";
                echo "<div class='container'><div class=\"alert alert-danger\" role=\"alert\">User not found !</div></div>";
            }
        }


    }
}

?>
</html>