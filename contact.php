<!DOCTYPE html>
<html>
	<head>
		<title>Contact</title>
		<link rel="stylesheet" type="text/css" href="css/onlineticket.css">
	</head>
	<body>
		<header>
			<div class = "container">
				<div id = "branding">
					<h1><span class ="highlight">Online</span>Ticket</h1>
				</div>	
				<nav>
					<ul>
						<li><a href = "onlineticket.php">Home</li>
						<li><a href = "about.php">About</li>
						<li class = "current"><a href = "#">Contact</li>	

					</ul>

				</nav>
			</div>	

		</header>

		
		<section id = "newslatter" >
			<div class = "container">
				
				<!-- <form >
					<input type="text" placeholder ="type..">
					<button type ="submit" class = "button_1">Submit</button>

				</form> -->

			</div>	
		</section>\

		<section id = "main">
			<div class = "container">
				<article id= "main-col">
					<h1 class = "page-title">Contact Us</h1>
					
					<ul id = "services">
						<li>
							
							<p>Tickethotline International Sdn Bhd (533610-D)
							No, 96b, 2nd Floor
							Jalan Burhanuddin Helmi, Taman Tun Dr.Ismail
							60000 Kuala Lumpur

							Tel :03-7725 1177 / 03 7725 1166
							Email : sales@tickethotline.com.my</p>


						</li>


				</article>
				<aside id = "sidebar">
					<div class = "dark">
						<h3>Get a Quote</h3>
							<form class = "quote">
								<div>
									<label>Name</label><br>
									<input type="text" placeholder="Name">

								</div>
								<div>
									<label>Email</label><br>
									<input type="email" placeholder="Email">

								</div>
								<div>
									<label>Message</label><br>
									<input type="text" placeholder="Message">

								</div>
								<button class = "buton_1" = type="submit">Send</button>
								
								
							</form>
						
					</div>
				</aside>
				
			</div> 
		</section>

		<footer>
			<p id = "foo">Online Ticket, Copyright &copy; 2019</p>

		</footer>



	</body>

</html>