<!DOCTYPE html>
<html>
	<head>
		<title>About</title>
		<link rel="stylesheet" type="text/css" href="css/onlineticket.css">
	</head>
	<body>
		
		<header>
			<div class = "container">
				<div id = "branding">
					<h1><span class ="highlight">Online</span>Ticket</h1>
				</div>	
				<nav>
					<ul>
						<li><a href = "onlineticket.php">Home</li>
						<li class = "current"><a href = "#">About</li>
						<li><a href = "contact.php">Contact</li>

					</ul>

				</nav>
			</div>	

		</header>

		
		<section id = "newslatter" >
			<div class = "container">
				
				<form >
					<input type="text" placeholder ="type..">
					<button type ="submit" class = "button_1">Submit</button>

				</form>

			</div>	
		</section>\

		<section id = "main">
			<div class = "container">
				<article id= "main-col">
					<h1 class = "page-title">About Us</h1>
					<p> Our mission is to offer event organizers a better alternative to the conventional methods of online ticketing and Access Control offered by other ticketing services.

					We strive to maintain a ticketing system that is easy to use to the event organizers while continuing to offer a broad range of ticketing, and reporting features via easy to use real time interface.

					We offer the highest level of ticketing service while maintaining a fee structure that charges less to the event patron compared to our competitors.

					We provide customized online ticketing management solution software for any event.

					It’s easy to subscribe as an event organizer, simply call us at +603 7725 1177 / +603 7725 1166.

					Take advantage of what e-Ticketing has to offer…</p>

					<p class = "dark">Automated online ticket sales available 24/7
					Advantage of e-ticket, M-ticket and SMS content
					Customized ticketing system and tickets.
					Lower fees than other online retailers.
					Manage multiple events from one easy to use account.
					Scalable site design allows for 
					</p>

				</article>
				<aside id = "sidebar">
					<div class = "dark">
						<h3>What we Do</h3>
						<p>Our mission is to offer event organizers a better alternative to the conventional methods of online ticketing and Access Control offered by other ticketing services.

						We strive to maintain a ticketing system that is easy to use to the event organizers while continuing to offer a broad range of ticketing, and reporting features via easy to use real time interface.</p>
					</div>
				</aside>
				
			</div> 
		</section>

		<footer>
			<p id = "foot">Online Ticket, Copyright &copy; 2019</p>

		</footer>



	</body>

</html>